/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import {Injectable, EventEmitter, Output} from '@angular/core';
import {Title} from '@angular/platform-browser';
import {CookieService} from 'angular2-cookie/core';


interface ShareObj {
    [id: string]: any;
}

@Injectable()
export class GlobalDataService {
    shareObj: ShareObj = {};
    @Output() userChangeEvent: EventEmitter<string> = new EventEmitter(true);
    @Output() selectTemplateChangeEvent: EventEmitter<any> = new EventEmitter(true);
    @Output() saveTemplateChangeEvent: EventEmitter<any> = new EventEmitter(true);
    apiURL = "";

    constructor(private title: Title, private _cookieService: CookieService) {
        this.shareObj['api'] = "http://119.59.115.182:8080/KDXAPI/index.php/";
        //this.shareObj['api'] = "http://http://183.88.239.144:8080/tiaraAPI/index.php/";
        this.apiURL = "http://119.59.115.182:8080/KDXAPI/";

        this.shareObj['configAPI'] = this.shareObj['api'] + "Config/";
        this.shareObj['userAPI'] = this.shareObj['api'] + "User/";
        this.shareObj['deviceAPI'] = this.shareObj['api'] + "XDK/";

    }


    setTitle(newTitle: string) {
        this.title.setTitle(newTitle);
    }

    getCookie(key: string) {
        return this._cookieService.get(key);
    }

    setCookie(key: string, value: any) {
        let expire = new Date();
        expire.setMinutes(expire.getMinutes() + 150);
        return this._cookieService.put(key, value, {expires: expire});
    }

    deleteCookie(key: string) {
        return this._cookieService.remove(key);
    }



    getCookieObject(key: string): any {
        return JSON.parse(this._cookieService.get(key));
    }

    setCookieObject(key: string, value: any) {
        let expire = new Date();
        expire.setMinutes(expire.getMinutes() + 150);
        return this._cookieService.put(key, JSON.stringify(value), {expires: expire});
    }


    setDefaultMultipleSelect(companies: any[]) {
        if (this.shareObj['selectedCompany'] != null && companies.indexOf(this.shareObj['selectedCompany']) < 0) {
            companies.push(this.shareObj['selectedCompany']);
        }
    }

    setCompany(company_id: number) {
        try {
            this.shareObj['main'].setCompany(company_id);
        } catch (e) {

        }
    }

    getSelectedCompany() {
        return this.shareObj['main'].getSelectedCompany();
    }


    showMSG(severity: string, summary: string, msg: string) {
        //this.saleComponent.showMSG(severity, summary, msg);
    }


    checkPermissionByUrl(url: string): boolean {
        let bReturn = false;
        if (this.shareObj['main'].reports == null) {
            return bReturn;
        }
        for (let r of this.shareObj['main'].reports) {
            if (r.url === url) {
                bReturn = true;
                break;
            }
        }
        return bReturn;
    }

    getDiffHour(sdate: Date, edate: Date): number {
        try {
            let diffMS = edate.getTime() - sdate.getTime();
            return Math.round(diffMS / 1000.0 / 60.0 / 60.0);
        }
        catch (e) {

        }

        try {
            sdate = new Date(sdate)
            edate = new Date(edate);
            let diffMS = edate.getTime() - sdate.getTime();
            return Math.round(diffMS / 1000.0 / 60.0 / 60.0);
        }
        catch (e) {

        }
    }

}