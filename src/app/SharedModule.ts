import {NgModule} from '@angular/core';

import {TEXTBOX_DIRECTIVES} from './directives';

@NgModule({
    declarations: [TEXTBOX_DIRECTIVES],
    exports: [TEXTBOX_DIRECTIVES]
})

export class SharedModule {}