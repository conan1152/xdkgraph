import {Component} from '@angular/core';

@Component({
    selector: 'app-header',
    templateUrl: './app-header.component.html'
})
export class AppHeaderComponent {

    ngAfterViewInit() {
        document.body.classList.remove('sidebar-hidden');
        document.body.classList.add('sidebar-hidden');
    }

}
