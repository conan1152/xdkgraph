export const navigation = [
    {
        name: 'Dashboard',
        url: '/dashboard',
        icon: 'icon-speedometer',
        badge: {
            variant: 'info',
            text: 'NEW'
        }
    },
    {
        name: 'Chart',
        url: '/chart/chart',
        icon: 'icon-speedometer'
    },
    {
        name: 'Master',
        url: '/masterdata',
        icon: 'icon-puzzle',
        children: [
            {
                name: 'Config',
                url: '/masterdata/config',
                icon: 'icon-puzzle'
            },
            {
                name: 'User',
                url: '/masterdata/user',
                icon: 'icon-puzzle'
            }
        ]
    }
];
