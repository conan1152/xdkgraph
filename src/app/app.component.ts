import {Component, OnInit} from '@angular/core';
import {Router, NavigationEnd} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';

@Component({
    // tslint:disable-next-line
    selector: 'body',
    template: '<router-outlet></router-outlet>'
})
export class AppComponent implements OnInit {
    constructor(private router: Router,private translateService: TranslateService) {}

    ngOnInit() {
         this.translateService.setDefaultLang('th');

        //the lang to use, if the lang isn't available, it will use the current loader to get them
        this.translateService.use('th');
        
        this.router.events.subscribe((evt) => {
            if (!(evt instanceof NavigationEnd)) {
                return;
            }
            
            console.log(evt);
            
            window.scrollTo(0, 0)
        });
    }
    
    
    
}
