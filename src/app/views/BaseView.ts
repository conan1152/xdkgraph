import {GlobalDataService} from '../../globaldata.service';
import {UnderscoreStatic} from 'underscore';
import * as _ from 'underscore';
declare var $: any;

export class BaseView {

    public _: UnderscoreStatic;
    public $: any;
    public RD_TYPE_ORDER = 1;
    public RD_TYPE_RECEIPT = 2;
    public RD_TYPE_CUSTOMER = 3;
    public RD_TYPE_ITEM = 4;
    public RD_TYPE_STOCK = 5;
    public RD_TYPE_TRANSACTION = 6;
    public RD_TYPE_RESERVATION = 7;

    constructor(private bgd: GlobalDataService) {
        this._ = _;
        this.$ = $;
    }

    getBooleanFromString(value: any): boolean {
        let bReturn = false;

        if (value != null) {
            if (value === "t" || value == true || value === "True" || value === "true") {
                bReturn = true;
            }
        }

        return bReturn;
    }

    getSaveDateFormat(d: Date): string {

        if (!(d instanceof Date)) {
            try {
                d = new Date(d);
            } catch (e) {
                return "";
            }

        }
        let sReturn = "";


        sReturn = d.getFullYear() + "/" + this.pad((d.getMonth() + 1)) + "/" + this.pad(d.getDate());

        return sReturn;

    }

    getSaveDateTimeFormat(d: Date): string {

        let sReturn = "";


        sReturn = d.getFullYear() + "/" + (d.getMonth() + 1) + "/" + d.getDate() + " " + d.getHours() + ":" + d.getMinutes();

        return sReturn;

    }

    pad(n: any) {
        let size = 2;
        let s = n + "";
        while (s.length < (size || 2)) {s = "0" + s;}
        return s;
    }


    getRundocStringType(rdtype: number): string {
        rdtype = Number(rdtype);

        if (rdtype === this.RD_TYPE_ORDER) {
            return "Order";
        }

        if (rdtype === this.RD_TYPE_RECEIPT) {
            return "Receipt";
        }

        if (rdtype === this.RD_TYPE_CUSTOMER) {
            return "Customer";
        }

        if (rdtype === this.RD_TYPE_ITEM) {
            return "Item";
        }

        if (rdtype === this.RD_TYPE_STOCK) {
            return "Stock";
        }

        if (rdtype === this.RD_TYPE_TRANSACTION) {
            return "Transaction";
        }

        if (rdtype === this.RD_TYPE_RESERVATION) {
            return "Appointment";
        }

        return "";
    }

    numberWithCommas(x: number) {
        if (x == null) {
            return 0.00;
        }

        let tmpNumber = Number(x).toFixed(2);
        let stmpNumber = tmpNumber.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        return stmpNumber.replace(".00", "");
    }

    csvJSON(csv):any {
        var lines = csv.split("\n");

        var result = [];

        var headers = lines[0].split(",");

        for (var i = 1; i < lines.length; i++) {

            var obj = {};
            var currentline = lines[i].split(",");

            for (var j = 0; j < headers.length; j++) {
                obj[headers[j]] = currentline[j];
            }

            result.push(obj);

        }

        //return result; //JavaScript object
        return JSON.stringify(result); //JSON
    }

}
