import {Component} from '@angular/core';
import {MasterService} from '../../../services/MasterService';
import {UserLoginRequest, UserObject} from '../../../services/MasterObject';
import {Router} from '@angular/router';
import {GlobalDataService} from '../../../globaldata.service';

@Component({
    templateUrl: 'login.component.html'
})
export class LoginComponent {

    username = "";
    password = "";

    constructor(private gd: GlobalDataService, private router: Router, private masterService: MasterService) {

    }

    login() {
        let req = new UserLoginRequest();
        req.username = this.username;
        req.password = this.password;

        this.masterService.getLogin(req).then(resData => {
            if (resData.status_code === "1") {
                this.gd.setCookie("username", resData.username);
                this.gd.setCookie("tblcompany_id", resData.tblcompany_id);
                this.router.navigateByUrl('/dashboard');
            }
        });
    }

}
