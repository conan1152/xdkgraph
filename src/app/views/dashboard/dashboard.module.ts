import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {ChartModule} from 'primeng/chart';
import {BsDropdownModule} from 'ngx-bootstrap/dropdown';
import {ButtonsModule} from 'ngx-bootstrap/buttons';
import {DashboardComponent} from './dashboard.component';
import {DashboardRoutingModule} from './dashboard-routing.module';
import {CommonModule} from "@angular/common";
import {ChartComponent} from './chart.component';
import {CalendarModule} from 'primeng/calendar';

@NgModule({
    imports: [
        FormsModule,
        DashboardRoutingModule,
        ChartModule,
        BsDropdownModule,
        ButtonsModule.forRoot(),
        CommonModule,
        CalendarModule
    ],
    declarations: [DashboardComponent, ChartComponent]
})
export class DashboardModule {}
