import {Component, OnInit, ElementRef, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {DeviceService} from '../../../services/DeviceService';
import {GlobalDataService} from '../../../globaldata.service';
import {DeviceStatusObject} from '../../../services/DeviceObject';
import {MasterService} from '../../../services/MasterService';
import {ConfigObject} from '../../../services/MasterObject';
import {BaseView} from '../BaseView';

declare let RadialGauge: any;
declare let LinearGauge: any;

@Component({
    templateUrl: 'chart.component.html'
})
export class ChartComponent extends BaseView implements OnInit {


    data: DeviceStatusObject[] = [];

    data2: any;
    config: ConfigObject = new ConfigObject();
    sdate: Date;
    edate: Date;

    tempData: any = new Object();
    humidityData: any = new Object();
    noiseData: any = new Object();

    constructor(private deviceService: DeviceService, private gd: GlobalDataService, private masterService: MasterService) {
        super(gd);
        this.sdate = new Date();
        this.edate = new Date();
    }

    ngOnInit(): void {
        this.data2 = {
            labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
            datasets: [
                {
                    label: 'First Dataset',
                    data: [65, 59, 80, 81, 56, 55, 40],
                    fill: false,
                    borderColor: '#4bc0c0'
                },
                {
                    label: 'Second Dataset',
                    data: [28, 48, 40, 19, 86, 27, 90],
                    fill: false,
                    borderColor: '#565656'
                }
            ]
        }

    }

    show() {
        let req: any = new Object();
        req.sdate = this.getSaveDateTimeFormat(this.sdate);
        let edate = new Date(this.edate.getTime());
        edate.setDate(edate.getDate() + 1);
        req.edate = this.getSaveDateTimeFormat(edate);
        this.deviceService.getDataRange(req).then(resData => {
            this.data = resData;
            this.initData();
        });
    }


    initData() {

        let x_length = 20;
        let determinator = this.data.length / x_length;

        let labels: string[] = [];
        let temp: number[] = [];
        let noise: number[] = [];
        let humidity: string[] = [];

        for (let i = 0; i < this.data.length; i += 20) {
            let d = this.data[i];
            labels.push(d.create_date);

            temp.push(d.temp);
            noise.push(d.noise);
            humidity.push(d.humidity);

        }

        this.tempData = {
            labels: labels,
            datasets: [
                {
                    label: 'Temperature',
                    data: temp,
                    fill: false,
                    borderColor: '#4bc0c0'
                }
            ]
        }

        this.humidityData = {
            labels: labels,
            datasets: [
                {
                    label: 'Humidity',
                    data: humidity,
                    fill: false,
                    borderColor: '#4bc0c0'
                }
            ]
        }

        this.noiseData = {
            labels: labels,
            datasets: [
                {
                    label: 'Noise',
                    data: noise,
                    fill: false,
                    borderColor: '#4bc0c0'
                }
            ]
        }
    }


}
