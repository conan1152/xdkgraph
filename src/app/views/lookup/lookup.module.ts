import {NgModule} from '@angular/core';
import {TableModule} from 'primeng/table';
import {CommonModule} from "@angular/common";
import {FormsModule} from '@angular/forms';
import {ModalModule} from 'ngx-bootstrap/modal';
import {CalendarModule} from 'primeng/calendar';
import {LookupRoutingModule} from './lookup-routing.module';


// Tabs Component
import {TabsModule} from 'ngx-bootstrap/tabs';


@NgModule({
    imports: [
        LookupRoutingModule,
        TabsModule,
        TableModule,
        CommonModule,
        FormsModule,
        ModalModule.forRoot(),
        CalendarModule
    ],
    declarations: [
        
    ],
    exports: [
        
    ]
})
export class LookupModule {}
