import {NgModule} from '@angular/core';
import {MasterRoutingModule} from './master-routing.module';
import {TableModule} from 'primeng/table';
import {CommonModule} from "@angular/common";
import {FormsModule} from '@angular/forms';
import {ModalModule} from 'ngx-bootstrap/modal';
import {CalendarModule} from 'primeng/calendar';
import {DropdownModule} from 'primeng/dropdown';
import {GaugeModule} from 'angular-gauge';

import {ConfigComponent} from './config.component';
import {UserComponent} from './user.component';

import {SharedModule} from '../../SharedModule';

@NgModule({
    imports: [
        MasterRoutingModule,
        TableModule,
        CommonModule,
        FormsModule,
        ModalModule.forRoot(),
        CalendarModule,
        SharedModule,
        DropdownModule,
        GaugeModule
    ],
    declarations: [
        ConfigComponent,
        UserComponent
    ]
})
export class MasterModule {}
