import {Component, ViewChild, ElementRef} from '@angular/core';
import {MasterService} from '../../../services/MasterService';
import {GlobalDataService} from '../../../globaldata.service';
import {UserObject, UserObjectSaveRequest} from '../../../services/MasterObject';
import {BaseView} from '../BaseView';

@Component({
    templateUrl: 'user.component.html'
})
export class UserComponent extends BaseView {

    @ViewChild('txtFirstEditControl') txtFirstEditControl: ElementRef;
    @ViewChild('editModal') editModal: any;

    lists: UserObject[] = [];
    cols: any[];
    row: UserObject = new UserObject();

    constructor(private masterService: MasterService, private gd: GlobalDataService) {
        super(gd);
    }

    ngOnInit() {
        this.cols = [
            {field: 'name', header: 'Name'}
        ];


        this.fillData();

    }

    fillData() {
        this.masterService.getUser().then(resData =>
            this.lists = resData
        );
    }

    showDialogToAdd() {
        this.row = new UserObject();
        this.editModal.show();
        setTimeout(() => this.txtFirstEditControl.nativeElement.focus(), 200);
    }

    save() {
        let req = new UserObjectSaveRequest();
        req.id = this.row.id;
        req.username = this.row.username;
        req.name = this.row.name;
        req.password = this.row.password;

        this.masterService.saveUserObjectRequest(req).then(resData => {
            if (resData.status_code === "0") {
                return;
            }
            if (req.id == null) {
                let user: UserObject = resData;
                this.lists.push(user);
            }
            this.editModal.hide();
        });
    }

    cancel() {
        this.editModal.hide();
    }

    showDialogToEdit(row: UserObject) {
        this.row = row;
        this.editModal.show();
    }

    deleteRow() {
        let req = new UserObjectSaveRequest();
        req.id = this.row.id;
        this.masterService.deleteUser(req).then(resData => {
            this.lists = this.lists.filter((val, i) => val.id != req.id);
        });
    }



    onRowSelect(event) {
        //        this.newCar = false;
        //        this.car = this.cloneCar(event.data);
        //        this.displayDialog = true;
    }

    selectRow(row: UserObject) {
        this.row = row;
    }


    edit(row: UserObject) {
        this.row = row;
        this.editModal.show();
        setTimeout(() => {
            this.txtFirstEditControl.nativeElement.focus();
            this.txtFirstEditControl.nativeElement.select();
        }, 200);
    }

}
