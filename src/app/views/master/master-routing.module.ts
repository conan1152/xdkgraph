import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ConfigComponent} from './config.component';
import {UserComponent} from './user.component';

const routes: Routes = [
    {
        path: '',
        data: {
            title: 'Master Data'
        },
        children: [
            {
                path: 'config',
                component: ConfigComponent,
                data: {
                    title: 'Config'
                }
            }
            ,
            {
                path: 'user',
                component: UserComponent,
                data: {
                    title: 'User management'
                }
            }

        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class MasterRoutingModule {}
