import {Component, ViewChild, ElementRef} from '@angular/core';
import {MasterService} from '../../../services/MasterService';
import {GlobalDataService} from '../../../globaldata.service';
import {ConfigObject} from '../../../services/MasterObject';
import {BaseView} from '../BaseView';

@Component({
    templateUrl: 'config.component.html'
})
export class ConfigComponent extends BaseView {

    @ViewChild('editModal') editModal: any;

    lists: ConfigObject[] = [];
    row: ConfigObject = new ConfigObject();


    constructor(private masterService: MasterService, private gd: GlobalDataService) {
        super(gd);
    }

    ngOnInit() {
        this.initData();
    }


    async initData() {

        this.fillData();
    }

    fillData() {
        this.masterService.getConfig().then(resData => {
            this.lists = resData;
        });
    }



    save() {
        let req = new ConfigObject();
        req = this._.clone(this.row);

        this.masterService.saveConfig(req).then(resData => {
            if (resData.status_code === "0") {
                return;
            }
            if (req.id == null) {
                this.lists.push(resData);
            }
            this.editModal.hide();
        });
    }

    cancel() {
        this.editModal.hide();
    }

    showDialogToEdit(row: ConfigObject) {
        this.row = row;
        this.editModal.show();
    }


    onRowSelect(event) {
        //        this.newCar = false;
        //        this.car = this.cloneCar(event.data);
        //        this.displayDialog = true;
    }

    selectRow(row: ConfigObject) {
        this.row = row;
    }


    edit(row: ConfigObject) {
        this.row = row;
        this.editModal.show();
    }



}
