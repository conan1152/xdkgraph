import {Directive, HostListener, ElementRef} from '@angular/core';

@Directive({
    selector: '[textBoxFocus]'
})
export class TextBoxFocusDirective {

    constructor(private el: ElementRef) {

    }

    @HostListener('focus') onFocus() {
        this.el.nativeElement.select();
    }

}



export const TEXTBOX_DIRECTIVES = [TextBoxFocusDirective];
