/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



export class UserLoginRequest {
    username: string;
    password: string;
}

export class UserObjectSaveRequest {
    id: number;
    username: string;
    password: string;
    name: string;

}

export class UserObject {
    id: number;
    username: string;
    password: string;
    name: string;
}


export class ConfigObject {
    id: number;
    min_humidity: number;
    max_humidity: number;
    min_temp: number;
    max_temp: number;
    min_noise: number;
    max_noise: number;
    warn_humidity:number;
    warn_temp:number;
    warn_noise:number;
}



