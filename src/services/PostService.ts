/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import {Injectable} from "@angular/core";
import {Http, Response, Headers} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {PostObject} from './PostObject';
import {GlobalDataService} from '../globaldata.service';

@Injectable()
export class PostService {
    constructor(private http: Http, private gd: GlobalDataService) {

    }

    post(url: string, req: any) {

        //        if (this.gd.getCookie("user") == null && url.indexOf("login") < 0) {
        //            //set to not logined
        //            this.gd.shareObj['main'].logined = false;
        //            return this.http.get(this.gd.shareObj['BaseApi'] + "emptyData")
        //                .toPromise()
        //                .then(this.extractData)
        //                .catch(this.handleError);
        //        }

        let headers = new Headers();
        headers.append('Content-Type',
            'application/x-www-form-urlencoded');
        let result = this.http.post(url, req, {
            headers: headers
            //      for keep session api
            , withCredentials: true
        })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);

        //        result.then(resData => {
        //            let postObjext: PostObject = resData;
        //            if (postObjext.resultCode != null && postObjext.resultCode == "0001"){
        //                 this.gd.shareObj['main'].logined = false;
        //            }
        //        });

        return result;
    }


    postSubSubscribe(url: string, req: any) {


        let headers = new Headers();
        headers.append('Content-Type',
            'application/x-www-form-urlencoded');
        let result = this.http.post(url, req, {headers: headers, withCredentials: true});
        //            .subscribe(response => {
        //                console.log(response);
        //                // if I am not mistaken "response.headers" should contain the "Set-Cookie" you are looking for 
        //            });



        return result;
    }

    private extractData(res: Response) {
        let body = res.json();

        try {
            if (body.status_code === "10") {
                window.location.href = "/#/pages/login";
                body = [];
            }
        } catch (e) {

        }

        return body || [];
    }
    private handleError(error: any) {
        // In a real world app, we might use a remote logging infrastructure
        // We'd also dig deeper into the error to get a better message
        try {
            let errMsg = (error.message) ? error.message :
                error.status ? `${error.status} - ${error.statusText}` : 'Server error';
            console.error(errMsg); // log to console instead
            //create error result object
            let result = new PostObject();
            result.msg = errMsg.status;
            result.resultCode = errMsg.status;
            result.detail = errMsg.statusText;
            return result;
        } catch (e) {
            console.log(error);
        }
        let result = new PostObject();
        result.msg = "Error"
        result.resultCode = "0010"
        result.detail = "No detail"
        return result;
    }
}

