import {Injectable} from '@angular/core';
import {GlobalDataService} from '../globaldata.service';
import {PostService} from './PostService';

import {UserObjectSaveRequest, UserLoginRequest, ConfigObject} from '../services/MasterObject';

@Injectable()
export class MasterService {

    userUrl: string;
    configUrl: string;
    deviceUrl: string;



    constructor(private gd: GlobalDataService, private ps: PostService) {
        this.userUrl = this.gd.shareObj['userAPI'];
        this.configUrl = this.gd.shareObj['configAPI'];
        this.deviceUrl = this.gd.shareObj['deviceAPI'];
    }


    saveUserObjectRequest(req: UserObjectSaveRequest) {
        return this.ps.post(this.userUrl + "editData", req);
    }

    getUser() {
        return this.ps.post(this.userUrl + "getData", {});
    }


    deleteUser(req: UserObjectSaveRequest) {
        return this.ps.post(this.userUrl + "deleteData", req);
    }

    getLogin(req: UserLoginRequest) {
        return this.ps.post(this.userUrl + "login", req);
    }

    getConfig() {
        return this.ps.post(this.configUrl + "getData", {});
    }

    saveConfig(req: ConfigObject) {
        return this.ps.post(this.configUrl + "editData", req);
    }



}