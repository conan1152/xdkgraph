import {Injectable} from '@angular/core';
import {GlobalDataService} from '../globaldata.service';
import {PostService} from './PostService';


@Injectable()
export class DeviceService {

    userUrl: string;
    configUrl: string;
    deviceUrl: string;



    constructor(private gd: GlobalDataService, private ps: PostService) {
        this.userUrl = this.gd.shareObj['userAPI'];
        this.configUrl = this.gd.shareObj['configAPI'];
        this.deviceUrl = this.gd.shareObj['deviceAPI'];
    }


    getCurrent() {
        return this.ps.post(this.deviceUrl + "getCurrent", {});
    }

    getDataRange(req: any) {
        return this.ps.post(this.deviceUrl + "getDataRange", req);
    }



}