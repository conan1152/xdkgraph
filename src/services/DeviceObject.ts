/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



export class DeviceStatusObject {
    id: number;
    device_name: number;
    humidity: string;
    temp: number;
    noise: number;
    ProjectCode: string;
    bat: string;
    create_date:string;
}
